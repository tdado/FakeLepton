#pragma once

#include "TEfficiency.h"
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"

#include <map>
#include <memory>
#include <string>
#include <vector>

class TPad;

class Processor {

public:

  explicit Processor() = default;

  ~Processor() = default;

  inline void SetInputFolder(const std::string& f){m_input_folder = f;}
  inline void SetOutputFolder(const std::string& f){m_output_folder = f;}
  inline void SetDataPath(const std::string& path){m_data_path = path;}
  inline void SetPromptPaths(const std::vector<std::string>& paths){m_prompt_paths = paths;}
  inline void SetFakePaths(const std::vector<std::string>& paths){m_fake_paths = paths;}
  inline void SetRegions(const std::vector<std::pair<std::string,std::string> >& regions){m_regions = regions;}
  inline void SetRealRegions(const std::vector<std::pair<std::string,std::string> >& regions){m_real_regions = regions;}
  inline void SetAtlasLabel(const std::string& label){m_atlas_label = label;}
  inline void SetCMELabel(const std::string& label){m_cme_label = label;}
  inline void SetReleaseLabel(const std::string& label){m_release_label = label;}
  inline void SetSuffix(const std::string& suffix) {m_suffix = suffix;}

  void Process();

private:

  void ReadInputFiles();

  void CloseInputFiles();

  void ProcessComposition() const;

  void ProcessSingleComposition(const std::pair<std::string, std::string>& region, const bool is_el, const bool is_loose) const;

  void InitMaps();

  std::unique_ptr<TH1D> MergeHistos(const std::string& name,
                                    const std::string& region,
                                    const bool is_el,
                                    const bool is_prompt,
                                    const bool is_loose) const;

  std::unique_ptr<TH2D> MergeHistos2D(const std::string& region,
                                      const bool is_el,
                                      const bool is_prompt,
                                      const bool is_loose) const;

  void PlotComposition(TH1D* hist, const std::pair<std::string, std::string>& region, const bool is_el, const bool is_loose) const;

  std::string GetCompositionLabel(const int i) const;

  void DrawLabels(TPad *pad, const float x, const float y) const;

  std::vector<std::pair<TEfficiency, std::pair<std::string, std::string> > > PlotRealEfficiencies(const bool is_el, const bool is_pt) const;

  void PlotRealEfficiencies2D(const bool is_el) const;

  void PlotFakeEfficiencies2D(const bool is_el) const;

  std::vector<std::pair<TEfficiency, std::pair<std::string, std::string> > > PlotFakeEfficiencies(const bool is_el, const bool is_pt) const;

  TEfficiency GetEfficiencies(TH1D* loose, TH1D* tight) const;

  void PlotEfficiencies(std::vector<std::pair<TEfficiency, std::pair<std::string, std::string> > >& efficiencies,
                        const bool is_el,
                        const bool is_pt,
                        const bool is_real) const;

  void PlotEfficiencies2D(const std::vector<std::unique_ptr<TH2D> >& efficiencies,
                          const std::vector<std::string>& labels,
                          const bool is_el,
                          const bool is_real) const;

  void PlotSingleEfficiency2D(TH2D* efficiencies,
                              const std::string& label,
                              const bool is_el,
                              const bool is_reali,
                              const bool is_smooth) const;

  void PlotCombined2DEfficiencies(std::vector<std::pair<TEfficiency, std::pair<std::string, std::string> > >& pt_efficiencies,
                                  std::vector<std::pair<TEfficiency, std::pair<std::string, std::string> > >& eta_efficiencies,
                                  const bool is_el,
                                  const bool is_real) const;

  void PlotSingleCombined2DEfficiency(TEfficiency& pt_eff,
                                      TEfficiency& eta_eff,
                                      const std::string& label,
                                      const bool is_el,
                                      const bool is_real) const;

  void CapAtZero(TH1D* hist) const;

  void CapAtZero(TH2D* hist) const;

  std::unique_ptr<TH1D> EfficiencyToHistogram(const TEfficiency& eff) const;

  void PlotDistribution(TH1D* data,
                        TH1D* prompt,
                        const std::string& region,
                        const bool is_el,
                        const bool is_pt,
                        const bool is_loose) const;

  void Correct2DHisto(TH2D* input) const;

  /**
   * @brief Create smooth version of the histogram with many bins in pT
   *
   * @param histo
   * @return std::unique_ptr<TH2D>
   */
  std::unique_ptr<TH2D> Smooth2Dhisto(const TH2D* histo) const;

  std::unique_ptr<TH1D> Smooth1Dhisto(const TH1D* histo) const;

  std::string m_input_folder;
  std::string m_output_folder;
  std::string m_data_path;
  std::vector<std::string> m_prompt_paths;
  std::vector<std::string> m_fake_paths;
  std::unique_ptr<TFile> m_data;
  std::vector<std::unique_ptr<TFile> > m_prompt_files;
  std::vector<std::unique_ptr<TFile> > m_fake_files;
  std::vector<std::pair<std::string, std::string> > m_regions;
  std::vector<std::pair<std::string, std::string> > m_real_regions;
  std::map<int, std::string> m_class_map;
  std::string m_atlas_label;
  std::string m_cme_label;
  std::string m_release_label;
  std::unique_ptr<TFile> m_out_file;
  std::string m_suffix;
};
