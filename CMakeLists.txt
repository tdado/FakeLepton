# Set the minimum CMake version required to build the project.
cmake_minimum_required( VERSION 3.6 )

# Silence some warnings on macOS with new CMake versions.
if( NOT ${CMAKE_VERSION} VERSION_LESS 3.9 )
   cmake_policy( SET CMP0068 NEW )
endif()

# Set the project's name and version.
project( FakeLepton )

# Set up the "C++ version" to use.
set( CMAKE_CXX_STANDARD_REQUIRED 17 CACHE STRING
   "Minimum C++ standard required for the build" )
set( CMAKE_CXX_STANDARD 17 CACHE STRING
   "C++ standard to use for the build" )
set( CMAKE_CXX_EXTENSIONS FALSE CACHE BOOL
   "(Dis)allow using compiler extensions" )

# Specify the install locations for libraries and binaries.
set( CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin )
set( CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib )
set( CMAKE_INSTALL_LIBDIR ${CMAKE_BINARY_DIR}/lib )  # Needed by ROOT_GENERATE_DICTIONARY()

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
set(CMAKE_POLICY_DEFAULT_CMP0077 NEW)

# Set the warning flag(s) to use.
set( CMAKE_CXX_FLAGS "-Wall -Wextra -Wshadow -pedantic -O3 -g" )

# Add ROOT system directory and require ROOT.
find_package( ROOT 6.32.00 REQUIRED COMPONENTS Core MathCore Graf Hist Gpad  )

file(GLOB ATLASUTILS_SOURCES "AtlasUtils/*.C" )
file(GLOB ATLASUTILS_LIB_HEADERS "AtlasUtils/*.h" )

add_library( AtlasUtils SHARED ${ATLASUTILS_LIB_HEADERS} ${ATLASUTILS_SOURCES} )
target_include_directories( AtlasUtils
   PUBLIC ${ROOT_INCLUDE_DIRS}
   $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}> $<INSTALL_INTERFACE:> )
set_target_properties( AtlasUtils PROPERTIES LINKER_LANGUAGE CXX)
target_link_libraries( AtlasUtils ${ROOT_LIBRARIES} )

file(GLOB SOURCES "Root/*.cc" )
file(GLOB LIB_HEADERS "FakeLepton/*.h" )

# Build the shared library.
add_library( FakeLepton SHARED ${LIB_HEADERS} ${SOURCES} )
target_include_directories( FakeLepton
   PUBLIC ${ROOT_INCLUDE_DIRS}
   $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}> $<INSTALL_INTERFACE:> )
target_link_libraries(FakeLepton AtlasUtils ${ROOT_LIBRARIES} )

# Helper macro for building the project's executables.
macro( FakeLepton_add_executable name )
  add_executable( ${name} ${ARGN} )
  target_include_directories( ${name} PUBLIC ${ROOT_INCLUDE_DIRS} )
  target_link_libraries( ${name} AtlasUtils FakeLepton ${ROOT_LIBRARIES} )
  install( TARGETS ${name}
    EXPORT FakeLeptonTargets
    RUNTIME DESTINATION bin )
endmacro( FakeLepton_add_executable )

FakeLepton_add_executable( process util/process.cc )

