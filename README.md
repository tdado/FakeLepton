# Code for plotting real and fake lepton efficiencies

This code uses output of [FakeLeptonFrame](https://gitlab.cern.ch/tdado/FakeLeptonFrame) class of FastFrames to make all sort of plots for real and fake efficencies.

## Compilation
You need a valid ROOT installation. On an lxplus-like machine you can do:
```
setupATLAS
asetup StatAnalysis,0.4.0
```

And then compile the code:
```
mkdir build 
cd build 
cmake ../
make
```

## Running the code
To run the code do:
```
./build/bin/process <input path> <output path>
```

Where `<input path>` is the path to the folder with the output of FastFrames and `<output path>` is the path to the output folder (will be created).

The code will produce 1D fake and real efficiencies for pt and eta (separately for electrons and muons) comapring the different selections defined in `util/process.cxx`.
Similarly, the code will create one folder for each real and fake region and produce 2D eta vs pt efficiencies, data vs prompt lepton prediction plots as well as IFF class plots for the MC fakes.
The list of the paths to the prompt and fake files for each process is provided in `util/process.cxx`.
2D histograms starting with "Combined" are histograms created by taking the geometric average in of 1D pt and eta efficiencies in each bin.
Finally, a ROOT file is created that stores all the efficiencies for all the regions.

