#include "FakeLepton/Processor.h"

#include "AtlasUtils/AtlasStyle.h"

#include "TError.h"

#include <iostream>
#include <string>
#include <vector>

int main(int argc, const char** argv) {

  if (argc != 3) {
    std::cerr << "Expecting 2 argument, but " << argc-1 << " arguments provided\n";
    return 1;
  }

  const std::string input = argv[1];
  const std::string output = argv[2];

  static const std::vector<std::string> prompt_paths = {"ttbar_PP8_FS_prompt.root",
                                                        "st_PP8_FS_prompt.root",
                                                        "st_tW_PP8_FS_prompt.root",
                                                        "wjets_Sherpa_FS_prompt.root",
                                                        "zjets_Sherpa_FS_prompt.root",
                                                        "zjets_low_mll_Sherpa_FS_prompt.root",
                                                        "vv_Sherpa_FS_prompt.root"};
  static const std::vector<std::string> fake_paths = {"ttbar_PP8_FS_fake.root",
                                                      "st_PP8_FS_fake.root",
                                                      "st_tW_PP8_FS_fake.root",
                                                      "wjets_Sherpa_FS_fake.root",
                                                      "zjets_Sherpa_FS_fake.root",
                                                      "zjets_low_mll_Sherpa_FS_fake.root",
                                                      "vv_Sherpa_FS_fake.root"};
  static const std::vector<std::pair<std::string, std::string> > regions = {{"4j1b_fake", "#geq 4j, #geq 1b, MET#leq 30, MWT#leq 30 GeV"},
                                                                            {"4j2b_fake", "#geq 4j, #geq 2b, MET#leq 30, MWT#leq 30 GeV"},
                                                                            {"5j1b_fake", "#geq 5j, #geq 1b, MET#leq 30, MWT#leq 30 GeV"},
                                                                            {"5j2b_fake", "#geq 5j, #geq 2b, MET#leq 30, MWT#leq 30 GeV"}};
  static const std::vector<std::pair<std::string, std::string> > real_regions = {{"real", "MET #geq 30 GeV"}};

  SetAtlasStyle();

  gErrorIgnoreLevel = kError;

  Processor processor{};

  processor.SetAtlasLabel("Internal");
  processor.SetCMELabel("#sqrt{s} = 13 TeV");
  processor.SetReleaseLabel("Release 25");
  //processor.SetSuffix("_NoMWT");
  processor.SetSuffix("");

  processor.SetInputFolder(input);
  processor.SetOutputFolder(output);
  processor.SetDataPath("Data.root");
  processor.SetPromptPaths(prompt_paths);
  processor.SetFakePaths(fake_paths);
  processor.SetRegions(regions);
  processor.SetRealRegions(real_regions);

  processor.Process();

  return 0;
}
