#include "FakeLepton/Processor.h"

#include "TCanvas.h"
#include "TColor.h"
#include "TGraphAsymmErrors.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TPad.h"
#include "TStyle.h"
#include "TSystem.h"

#include <iostream>

void Processor::Process() {

    gSystem->mkdir(m_output_folder.c_str());

    for (const auto& ireg : m_regions) {
        gSystem->mkdir((m_output_folder+"/"+ireg.first).c_str());
    }
    for (const auto& ireg : m_real_regions) {
        gSystem->mkdir((m_output_folder+"/"+ireg.first).c_str());
    }


    this->InitMaps();

    this->ReadInputFiles();

    this->ProcessComposition();

    m_out_file = std::make_unique<TFile>((m_output_folder + "/Efficiencies.root").c_str(), "RECREATE");

    static const std::vector<bool> is_el = {true, false};
    for (const auto el : is_el) {
        auto pt_real_eff = this->PlotRealEfficiencies(el, true);
        auto pt_fake_eff = this->PlotFakeEfficiencies(el, true);
        auto eta_real_eff = this->PlotRealEfficiencies(el, false);
        auto eta_fake_eff = this->PlotFakeEfficiencies(el, false);
        this->PlotCombined2DEfficiencies(pt_real_eff, eta_real_eff, el, true);
        this->PlotCombined2DEfficiencies(pt_fake_eff, eta_fake_eff, el, false);
        this->PlotRealEfficiencies2D(el);
        this->PlotFakeEfficiencies2D(el);

        for (const auto& ieff : pt_real_eff) {
            auto hist = this->EfficiencyToHistogram(ieff.first);
            auto smooth = this->Smooth1Dhisto(hist.get());
            const std::string name = std::string("Efficiency_1D_real_pt_") + (el ? "el_" : "mu_") + ieff.second.first + m_suffix;
            const std::string name_smooth = std::string("Efficiency_1D_real_pt_") + (el ? "el_" : "mu_") + ieff.second.first + "_smooth" + m_suffix;
            m_out_file->cd();
            hist->Write(name.c_str());
            smooth->Write(name_smooth.c_str());
        }
        for (const auto& ieff : pt_fake_eff) {
            auto hist = this->EfficiencyToHistogram(ieff.first);
            auto smooth = this->Smooth1Dhisto(hist.get());
            const std::string name = std::string("Efficiency_1D_fake_pt_") + (el ? "el_" : "mu_") + ieff.second.first + m_suffix;
            const std::string name_smooth = std::string("Efficiency_1D_fake_pt_") + (el ? "el_" : "mu_") + ieff.second.first + "_smooth" + m_suffix;
            m_out_file->cd();
            hist->Write(name.c_str());
            smooth->Write(name_smooth.c_str());
        }
        for (const auto& ieff : eta_real_eff) {
            auto hist = this->EfficiencyToHistogram(ieff.first);
            auto smooth = this->Smooth1Dhisto(hist.get());
            const std::string name = std::string("Efficiency_1D_real_eta_") + (el ? "el_" : "mu_") + ieff.second.first + m_suffix;
            const std::string name_smooth = std::string("Efficiency_1D_real_eta_") + (el ? "el_" : "mu_") + ieff.second.first + "_smooth" + m_suffix;
            m_out_file->cd();
            hist->Write(name.c_str());
            smooth->Write(name_smooth.c_str());
        }
        for (const auto& ieff : eta_fake_eff) {
            auto hist = this->EfficiencyToHistogram(ieff.first);
            auto smooth = this->Smooth1Dhisto(hist.get());
            const std::string name = std::string("Efficiency_1D_fake_eta_") + (el ? "el_" : "mu_") + ieff.second.first + m_suffix;
            const std::string name_smooth = std::string("Efficiency_1D_fake_eta_") + (el ? "el_" : "mu_") + ieff.second.first + "_smooth" + m_suffix;
            m_out_file->cd();
            hist->Write(name.c_str());
            smooth->Write(name_smooth.c_str());
        }
    }

    m_out_file->Close();

    this->CloseInputFiles();
}

void Processor::ReadInputFiles() {
    if (!m_data_path.empty()) {
        m_data.reset(TFile::Open((m_input_folder + "/" + m_data_path).c_str(), "READ"));
        if (!m_data) {
            std::cerr << "Cannot read file at: " << m_input_folder + "/" + m_data_path << "\n";
            return;
        }
    }

    for (const auto& iprompt : m_prompt_paths) {
        const std::string path = m_input_folder + "/" + iprompt;
        std::unique_ptr<TFile> f(TFile::Open(path.c_str(), "READ"));
        if (!f) {
            std::cerr << "Cannot read file at: " << path << "\n";
            return;
        }
        m_prompt_files.emplace_back(std::move(f));
    }
    for (const auto& iprompt : m_fake_paths) {
        const std::string path = m_input_folder + "/" + iprompt;
        std::unique_ptr<TFile> f(TFile::Open(path.c_str(), "READ"));
        if (!f) {
            std::cerr << "Cannot read file at: " << path << "\n";
            return;
        }
        m_fake_files.emplace_back(std::move(f));
    }

    if (m_prompt_files.empty() || m_fake_files.empty()) {
        std::cerr << "Empty prompt or fale files\n";
        return;
    }
}

void Processor::CloseInputFiles() {
    if (m_data) m_data->Close();

    for (auto& ifile : m_prompt_files) {
        ifile->Close();
    }
    for (auto& ifile : m_fake_files) {
        ifile->Close();
    }
}

void Processor::ProcessComposition() const {
    for (const auto& ireg : m_regions) {
        this->ProcessSingleComposition(ireg, true, true);
        this->ProcessSingleComposition(ireg, false, true);
        this->ProcessSingleComposition(ireg, true, false);
        this->ProcessSingleComposition(ireg, false, false);
    }
}

void Processor::ProcessSingleComposition(const std::pair<std::string, std::string>& region, const bool is_el, const bool is_loose) const {

    std::unique_ptr<TH1D> merged_histo = this->MergeHistos("IFF", region.first, is_el, false, is_loose);
    if (!merged_histo) {
        std::cerr << "Cannot plot composition\n";
        return;
    }
    this->PlotComposition(merged_histo.get(), region, is_el, is_loose);
}

std::unique_ptr<TH1D> Processor::MergeHistos(const std::string& histo_name,
                                             const std::string& region,
                                             const bool is_el,
                                             const bool is_prompt,
                                             const bool is_loose) const {

    const std::string name = std::string("NOSYS/") + (is_el ? std::string("el_") : std::string("mu_")) + histo_name  + "_" + (is_loose ? "loose_" : "tight_") + region;
    std::unique_ptr<TH1D> hist(nullptr);
    for (const auto& ifile : (is_prompt ? m_prompt_files : m_fake_files)) {
        if (hist) {
            std::unique_ptr<TH1D> h(ifile->Get<TH1D>(name.c_str()));
            if (!h) {
                std::cerr << "Cannot read histo: " << name << "\n";
                return nullptr;
            }
            hist->Add(h.get());
        } else {
            hist.reset(ifile->Get<TH1D>(name.c_str()));
            if (!hist) {
                std::cerr << "Cannot read histo: " << name << "\n";
                return nullptr;
            }
        }
    }

    return hist;
}

std::unique_ptr<TH2D> Processor::MergeHistos2D(const std::string& region,
                                               const bool is_el,
                                               const bool is_prompt,
                                               const bool is_loose) const {

    const std::string name = std::string("NOSYS/") + (is_el ? std::string("el_") : std::string("mu_")) + "pt_" + (is_loose ? "loose_" : "tight_") + "vs_" + (is_el ? "el_" : "mu_") + "eta_" + (is_loose ? "loose_" : "tight_") + region;
    std::unique_ptr<TH2D> hist(nullptr);
    for (const auto& ifile : (is_prompt ? m_prompt_files : m_fake_files)) {
        if (hist) {
            std::unique_ptr<TH2D> h(ifile->Get<TH2D>(name.c_str()));
            if (!h) {
                std::cerr << "Cannot read histo: " << name << "\n";
                return nullptr;
            }
            hist->Add(h.get());
        } else {
            hist.reset(ifile->Get<TH2D>(name.c_str()));
            if (!hist) {
                std::cerr << "Cannot read histo: " << name << "\n";
                return nullptr;
            }
        }
    }

    return hist;
}

void Processor::InitMaps() {
    m_class_map.insert({0, "Unknown"});
    m_class_map.insert({1, "KnownUnknown"});
    m_class_map.insert({2, "Prompt electron"});
    m_class_map.insert({3, "Prompt electron CF"});
    m_class_map.insert({4, "Prompt muon"});
    m_class_map.insert({5, "Photon conversion"});
    m_class_map.insert({6, "Muon mis-id as electron"});
    m_class_map.insert({7, "Tau decay"});
    m_class_map.insert({8, "B-hadron decay"});
    m_class_map.insert({9, "C-hadron decay"});
    m_class_map.insert({10, "Light flavour decay"});
    m_class_map.insert({11, "Prompt muon CF"});
}

void Processor::PlotComposition(TH1D* histo, const std::pair<std::string, std::string>& region, const bool is_el, const bool is_loose) const {

    for (int ibin = 1; ibin <= histo->GetNbinsX(); ++ibin) {
        histo->GetXaxis()->SetBinLabel(ibin, this->GetCompositionLabel(ibin-1).c_str());
    }
    histo->GetXaxis()->SetTitle("");
    histo->GetYaxis()->SetTitle("Events");
    histo->GetYaxis()->SetTitleOffset(1.3*histo->GetYaxis()->GetTitleOffset());
    histo->LabelsOption("v");
    histo->SetMinimum(1);
    histo->SetMaximum(1e2*histo->GetMaximum());

    TCanvas c("", "", 800, 800);
    c.SetBottomMargin(0.3);
    histo->Draw("HIST");

    this->DrawLabels(&c, 0.2, 0.88);

    TLatex l1;
    l1.SetTextAlign(9);
    l1.SetTextSize(0.04);
    l1.SetTextFont(42);
    l1.SetNDC();
    l1.DrawLatex(0.6, 0.85, is_el ? "Electron" : "Muon");
    l1.DrawLatex(0.6, 0.80, is_loose ? "Loose" : "Tight");

    c.SetLogy();

    const std::string path = m_output_folder + "/" + region.first+"/IFF_classification_" + (is_el ? "el_" : "mu_") + (is_loose ? "loose" : "tight");
    c.Print((path+".png").c_str());
}

std::string Processor::GetCompositionLabel(const int i) const {
    auto itr = m_class_map.find(i);
    if (itr == m_class_map.end()) {
        std::cerr << "Cannot find index: " << i << " in the composition map\n";
        return "XXX";
    }

    return itr->second;
}

void Processor::DrawLabels(TPad *pad, const float x, const float y) const {
    pad->cd();

    TLatex l1;
    l1.SetTextAlign(9);
    l1.SetTextFont(72);
    l1.SetTextSize(0.04);
    l1.SetNDC();
    l1.DrawLatex(x, y, "ATLAS");

    TLatex l2;
    l2.SetTextAlign(9);
    l2.SetTextSize(0.04);
    l2.SetTextFont(42);
    l2.SetNDC();
    l2.DrawLatex(x+0.15, y, m_atlas_label.c_str());

    TLatex l3;
    l3.SetTextAlign(9);
    l3.SetTextSize(0.04);
    l3.SetTextFont(42);
    l3.SetNDC();
    l3.DrawLatex(x, y-0.05, m_cme_label.c_str());
    l3.DrawLatex(x, y-0.12, m_release_label.c_str());
}

std::vector<std::pair<TEfficiency, std::pair<std::string, std::string> > > Processor::PlotRealEfficiencies(const bool is_el, const bool is_pt) const {
    std::cout << "Plotting real efficiencies: is_el: " << is_el << ", is_pt: " << is_pt <<"\n";

    std::vector<std::pair<TEfficiency, std::pair<std::string, std::string> > > efficiencies;

    for (const auto& ireg : m_real_regions) {
        auto histo_loose = this->MergeHistos(is_pt ? "pt" : "eta", ireg.first, is_el, true, true);
        auto histo_tight = this->MergeHistos(is_pt ? "pt" : "eta", ireg.first, is_el, true, false);
        if (!histo_loose || !histo_tight) {
            std::cerr << "Cannot plot real efficiencies\n";
            return {};
        }
        efficiencies.emplace_back(std::make_pair(this->GetEfficiencies(histo_loose.get(), histo_tight.get()), ireg));
    }

    this->PlotEfficiencies(efficiencies, is_el, is_pt, true);
    return efficiencies;
}

void Processor::PlotRealEfficiencies2D(const bool is_el) const {
    std::cout << "Plotting real 2D efficiencies: is_el: " << is_el <<"\n";

    std::vector<std::unique_ptr<TH2D> > efficiencies;
    std::vector<std::string> names;
    for (const auto& ireg : m_real_regions) {
        auto histo_loose = this->MergeHistos2D(ireg.first, is_el, true, true);
        auto histo_tight = this->MergeHistos2D(ireg.first, is_el, true, false);
        if (!histo_loose || !histo_tight) {
            std::cerr << "Cannot plot real 2D efficiencies\n";
            return;
        }
        histo_tight->Divide(histo_loose.get());
        efficiencies.emplace_back(std::move(histo_tight));
        names.emplace_back(ireg.first);
    }
    this->PlotEfficiencies2D(efficiencies, names, is_el, true);
}

std::vector<std::pair<TEfficiency, std::pair<std::string, std::string> > > Processor::PlotFakeEfficiencies(const bool is_el, const bool is_pt) const {
    std::cout << "Plotting fake efficiencies: is_el: " << is_el << ", is_pt: " << is_pt <<"\n";
    if (!m_data) {
        std::cerr << "Data file is nullptr\n";
        return {};
    }

    std::vector<std::pair<TEfficiency, std::pair<std::string, std::string> > > efficiencies;

    for (const auto& iregion : m_regions) {
        auto Name = [is_el, is_pt, &iregion](const bool is_tight) {
            return std::string("NOSYS/") + (is_el ? "el_" : "mu_") + (is_pt ? "pt_" : "eta_") + (is_tight ? "tight_" : "loose_") + iregion.first;
        };

        std::unique_ptr<TH1D> data_tight(m_data->Get<TH1D>(Name(true).c_str()));
        std::unique_ptr<TH1D> data_loose(m_data->Get<TH1D>(Name(false).c_str()));

        if (!data_loose || !data_tight) {
            std::cerr << "Cannot read data histograms\n";
            return {};
        }

        auto prompt_tight = this->MergeHistos(is_pt ? "pt" : "eta", iregion.first, is_el, true, false);
        auto prompt_loose = this->MergeHistos(is_pt ? "pt" : "eta", iregion.first, is_el, true, true);

        if (!prompt_loose || !prompt_tight) {
            std::cerr << "Cannot read prompt histogmras\n";
            return {};
        }

        this->PlotDistribution(data_tight.get(), prompt_tight.get(), iregion.first, is_el, is_pt, false);
        this->PlotDistribution(data_loose.get(), prompt_loose.get(), iregion.first, is_el, is_pt, true);

        // subtract prompt from data
        data_tight->Add(prompt_tight.get(), -1.);
        data_loose->Add(prompt_loose.get(), -1.);

        this->CapAtZero(data_tight.get());
        this->CapAtZero(data_loose.get());

        efficiencies.emplace_back(std::make_pair(this->GetEfficiencies(data_loose.get(), data_tight.get()), iregion));
    }

    this->PlotEfficiencies(efficiencies, is_el, is_pt, false);
    return efficiencies;
}

void Processor::PlotFakeEfficiencies2D(const bool is_el) const {
    std::cout << "Plotting fake 2D efficiencies: is_el: " << is_el <<"\n";

    if (!m_data) {
        std::cerr << "Data file is nullptr\n";
        return;
    }

    std::vector<std::string> names;

    std::vector<std::unique_ptr<TH2D> > efficiencies;
    for (const auto& ireg : m_regions) {
        auto Name = [is_el, &ireg](const bool is_loose) {
            return std::string("NOSYS/") + (is_el ? std::string("el_") : std::string("mu_")) + "pt_" + (is_loose ? "loose_" : "tight_") + "vs_" + (is_el ? "el_" : "mu_") + "eta_" + (is_loose ? "loose_" : "tight_") + ireg.first;
        };

        std::unique_ptr<TH2D> data_tight(m_data->Get<TH2D>(Name(false).c_str()));
        std::unique_ptr<TH2D> data_loose(m_data->Get<TH2D>(Name(true).c_str()));

        if (!data_loose || !data_tight) {
            std::cerr << "Cannot read 2D data histograms\n";
            return;
        }

        auto histo_loose = this->MergeHistos2D(ireg.first, is_el, true, true);
        auto histo_tight = this->MergeHistos2D(ireg.first, is_el, true, false);
        if (!histo_loose || !histo_tight) {
            std::cerr << "Cannot plot real 2D efficiencies\n";
            return;
        }
        data_tight->Add(histo_tight.get(), -1.);
        data_loose->Add(histo_loose.get(), -1.);

        this->CapAtZero(data_tight.get());

        this->CapAtZero(data_loose.get());

        data_tight->Divide(data_loose.get());

        this->Correct2DHisto(data_tight.get());

        efficiencies.emplace_back(std::move(data_tight));
        names.emplace_back(ireg.first);
    }
    this->PlotEfficiencies2D(efficiencies, names, is_el, false);
}

TEfficiency Processor::GetEfficiencies(TH1D* loose, TH1D* tight) const {
    loose->SetBinContent(0,0);
    tight->SetBinContent(0,0);
    loose->SetBinContent(loose->GetNbinsX(), loose->GetBinContent(loose->GetNbinsX()) + loose->GetBinContent(loose->GetNbinsX()+1));
    tight->SetBinContent(tight->GetNbinsX(), tight->GetBinContent(tight->GetNbinsX()) + tight->GetBinContent(tight->GetNbinsX()+1));
    loose->SetBinContent(loose->GetNbinsX()+1, 0);
    tight->SetBinContent(tight->GetNbinsX()+1, 0);
    if (!TEfficiency::CheckConsistency(*tight, *loose)) {
        std::cout << "tight:\n";
        for (int ibin = 0; ibin <= tight->GetNbinsX()+1; ++ibin) {
            std::cout << "ibin: " << ibin << ", content: " << tight->GetBinContent(ibin) << "\n";
        }
        std::cout << "loose:\n";
        for (int ibin = 0; ibin <= loose->GetNbinsX()+1; ++ibin) {
            std::cout << "ibin: " << ibin << ", content: " << loose->GetBinContent(ibin) << "\n";
        }
        std::exit(EXIT_FAILURE);
    }

    return TEfficiency(*tight, *loose);
}

void Processor::PlotEfficiencies(std::vector<std::pair<TEfficiency, std::pair<std::string, std::string> > >& efficiencies,
                                 const bool is_el,
                                 const bool is_pt,
                                 const bool is_real) const {

    if (efficiencies.empty()) return;

    static const std::vector<int> colors = {kRed, kBlue, kGreen+2, kPink};
    static const std::vector<int> style  = {2, 3, 4, 5, 6};

    const std::string title = std::string(";") + (is_el ? "electron" : "muon") + (is_pt ? " p_{T} [GeV]" : " #eta") + ";" + (is_real ? "Real " : "Fake ") + "efficiency";
    TCanvas c("", "", 800, 600);
    if (is_pt) {
        c.SetLogx();
    }

    efficiencies.at(0).first.SetLineColor(kBlack);
    efficiencies.at(0).first.SetLineStyle(1);
    efficiencies.at(0).first.SetLineWidth(2);
    efficiencies.at(0).first.SetTitle(title.c_str());
    efficiencies.at(0).first.Draw("AP");
    gPad->Update();
    auto graph = efficiencies.at(0).first.GetPaintedGraph();
    graph->SetMinimum(0.);
    graph->SetMaximum(1.4);
    gPad->Update();
    for (std::size_t i = 1; i < efficiencies.size(); ++i) {
        efficiencies.at(i).first.SetLineColor(colors.at(i % 4));
        efficiencies.at(i).first.SetLineStyle(style.at(i % 5));
        efficiencies.at(i).first.SetLineWidth(2);
        efficiencies.at(i).first.Draw("same");
    }

    TLegend leg(0.45, 0.8, 0.8, 0.92);
    leg.SetBorderSize(0);
    leg.SetTextSize(0.035);
    leg.SetFillColor(0);
    leg.SetLineColor(0);
    leg.SetTextFont(42);
    for (std::size_t i = 0; i < efficiencies.size(); ++i) {
        leg.AddEntry(&efficiencies.at(i).first, efficiencies.at(i).second.second.c_str(), "l");
    }
    leg.Draw("same");

    this->DrawLabels(&c, 0.2, 0.9);

    const std::string path = m_output_folder + "/" + (is_real ? "Real_" : "Fake_") + "efficiency_" + (is_el ? "el_" : "mu_") + (is_pt ? "pt" : "eta");
    c.Print((path+".png").c_str());
}

void Processor::PlotEfficiencies2D(const std::vector<std::unique_ptr<TH2D> >& efficiencies,
                                   const std::vector<std::string>& labels,
                                   const bool is_el,
                                   const bool is_real) const {

    if (efficiencies.empty()) return;
    if (efficiencies.size() != labels.size()) {
        std::cerr << "Size of labels and 2D efficiencies do not match\n";
        return;
    }

    for (std::size_t i = 0; i < labels.size(); ++i) {
        this->PlotSingleEfficiency2D(efficiencies.at(i).get(), labels.at(i), is_el, is_real, false);
        auto smooth = this->Smooth2Dhisto(efficiencies.at(i).get());
        this->PlotSingleEfficiency2D(smooth.get(), labels.at(i), is_el, is_real, true);
    }
}

void Processor::PlotCombined2DEfficiencies(std::vector<std::pair<TEfficiency, std::pair<std::string, std::string> > >& pt_efficiencies,
                                           std::vector<std::pair<TEfficiency, std::pair<std::string, std::string> > >& eta_efficiencies,
                                           const bool is_el,
                                           const bool is_real) const {

    std::cout << "Plotting combined 2D efficiency, is_el: " << is_el << ", is real: " << is_real << "\n";

    if (pt_efficiencies.empty() || eta_efficiencies.empty()) {
        std::cerr << "Pt or Eta efficiencies are empty\n";
        return;
    }

    if (pt_efficiencies.size() != eta_efficiencies.size()) {
        std::cerr << "Pt or Eta efficiencies sizes do not match\n";
        return;
    }

    for (std::size_t i = 0; i < pt_efficiencies.size(); ++i) {
        this->PlotSingleCombined2DEfficiency(pt_efficiencies.at(i).first, eta_efficiencies.at(i).first, pt_efficiencies.at(i).second.first, is_el, is_real);
    }
}

void Processor::PlotSingleCombined2DEfficiency(TEfficiency& pt_eff,
                                               TEfficiency& eta_eff,
                                               const std::string& label,
                                               const bool is_el,
                                               const bool is_real) const {

    auto histo_pt = pt_eff.GetTotalHistogram();
    auto histo_eta = eta_eff.GetTotalHistogram();

    std::vector<double> bins_x;
    std::vector<double> bins_y;

    for (int ibin = 1; ibin <= (histo_pt->GetNbinsX() + 1); ++ibin) {
        bins_x.emplace_back(histo_pt->GetBinLowEdge(ibin));
    }
    for (int ibin = 1; ibin <= (histo_eta->GetNbinsX() + 1); ++ibin) {
        bins_y.emplace_back(histo_eta->GetBinLowEdge(ibin));
    }

    TH2D histo_2d("","", bins_x.size()-1, bins_x.data(), bins_y.size()-1, bins_y.data());
    for (int ibin = 1; ibin <= histo_pt->GetNbinsX(); ++ibin) {
        for (int jbin = 1; jbin <= histo_eta->GetNbinsX(); ++jbin) {
            histo_2d.SetBinContent(ibin, jbin, std::sqrt(pt_eff.GetEfficiency(ibin) * eta_eff.GetEfficiency(jbin)));
        }
    }

    const std::string title_x = (is_el ? std::string("electron") : std::string("muon")) + " p_{T} [GeV]";
    const std::string title_y = (is_el ? std::string("electron") : std::string("muon")) + " #eta";
    const std::string title_z = (is_real ? std::string("Real") : std::string("Fake")) + " efficiency";

    histo_2d.GetXaxis()->SetTitle(title_x.c_str());
    histo_2d.GetYaxis()->SetTitle(title_y.c_str());
    histo_2d.GetZaxis()->SetTitle(title_z.c_str());
    histo_2d.GetZaxis()->SetTitleOffset(1.7*histo_2d.GetZaxis()->GetTitleOffset());
    histo_2d.GetZaxis()->SetRangeUser(0,1);

    TCanvas c("","",800,600);
    c.SetRightMargin(0.2);
    c.SetLogx();

    gStyle->SetPaintTextFormat("1.1e");
    histo_2d.SetMarkerSize(700);
    histo_2d.Draw("COLZH TEXT45 ERROR");

    const std::string path = m_output_folder + "/" + label + "/Combined_efficiency_" + (is_el ? "el_" : "mu_") + (is_real ? "real" : "fake");
    c.Print((path+".png").c_str());
}

void Processor::PlotSingleEfficiency2D(TH2D* efficiencies,
                                       const std::string& label,
                                       const bool is_el,
                                       const bool is_real,
                                       const bool is_smooth) const {

    const std::string title_x = (is_el ? std::string("electron") : std::string("muon")) + " p_{T} [GeV]";
    const std::string title_y = (is_el ? std::string("electron") : std::string("muon")) + " #eta";
    const std::string title_z = (is_real ? std::string("Real") : std::string("Fake")) + " efficiency";

    efficiencies->GetXaxis()->SetTitle(title_x.c_str());
    efficiencies->GetYaxis()->SetTitle(title_y.c_str());
    efficiencies->GetZaxis()->SetTitle(title_z.c_str());
    efficiencies->GetZaxis()->SetTitleOffset(1.7* efficiencies->GetZaxis()->GetTitleOffset());
    efficiencies->GetZaxis()->SetRangeUser(0,1);

    TCanvas c("","",800,600);
    c.SetRightMargin(0.2);
    c.SetLogx();

    gStyle->SetPaintTextFormat("1.2f");
    efficiencies->SetMarkerSize(700);
    if (is_smooth) {
        efficiencies->Draw("COLZ");
    } else {
        efficiencies->Draw("COLZH TEXT45 ERROR");
    }

    std::string path = m_output_folder + "/" + label + "/2D_efficiency_" + (is_el ? "el_" : "mu_") + (is_real ? "real" : "fake");
    if (is_smooth) {
        path += "_smooth";
    }
    c.Print((path+".png").c_str());

    std::string name = std::string("Efficiency_2D_") + (is_real ? "real_" : "fake_") + (is_el ? "el_" : "mu_") + label;
    if (is_smooth) {
        name += "_smooth";
    }
    name += m_suffix;
    m_out_file->cd();
    efficiencies->Write(name.c_str());
}

std::unique_ptr<TH1D> Processor::EfficiencyToHistogram(const TEfficiency& eff) const {
    auto graph = eff.GetTotalHistogram();
    auto graph1 = eff.CreateGraph();

    const int nbins = graph->GetNbinsX();
    std::vector<double> edges;
    for (int ibin = 1; ibin <= nbins+1; ++ibin) {
        const double edge = graph->GetXaxis()->GetBinLowEdge(ibin);
        edges.emplace_back(edge);
    }

    std::unique_ptr<TH1D> histo = std::make_unique<TH1D>("","", edges.size() - 1, edges.data());
    histo->SetDirectory(nullptr);
    for (int ibin = 1; ibin <= nbins; ++ibin) {
        double x;
        double y;
        graph1->GetPoint(ibin-1, x, y);
        histo->SetBinContent(ibin, y);
        histo->SetBinError(ibin, graph1->GetErrorY(ibin-1));
    }

    return histo;
}

void Processor::CapAtZero(TH1D* hist) const {
    for (int ibin = 0; ibin <= hist->GetNbinsX() + 1; ++ibin) {
        if (hist->GetBinContent(ibin) < 0) hist->SetBinContent(ibin, 0);
    }
}

void Processor::CapAtZero(TH2D* hist) const {
    for (int ibin = 0; ibin <= hist->GetNbinsX() + 1; ++ibin) {
        for (int jbin = 0; jbin <= hist->GetNbinsY() + 1; ++jbin) {
            if (hist->GetBinContent(ibin, jbin) < 0) hist->SetBinContent(ibin, jbin, 0);
        }
    }
}

void Processor::PlotDistribution(TH1D* data,
                                 TH1D* prompt,
                                 const std::string& region,
                                 const bool is_el,
                                 const bool is_pt,
                                 const bool is_loose) const {

    TCanvas c("","", 800, 600);

    data->SetLineColor(kBlack);
    prompt->SetFillColor(kRed);
    prompt->SetLineColor(kRed);

    prompt->GetYaxis()->SetRangeUser(0, 1.3*data->GetMaximum());

    prompt->Draw("HIST");
    data->Draw("PE same");

    if (is_pt) {
        c.SetLogx();
    }

    TLegend leg(0.5, 0.8, 0.9, 0.92);
    leg.SetBorderSize(0);
    leg.SetTextSize(0.035);
    leg.SetFillColor(0);
    leg.SetLineColor(0);
    leg.SetTextFont(42);
    leg.AddEntry(data, "Data", "p");
    leg.AddEntry(prompt, "Prompt MC", "l");
    leg.Draw("same");

    this->DrawLabels(&c, 0.2, 0.9);

    const std::string path = m_output_folder + "/" + region + "/Data_prediction_" + (is_el ? "el_" : "mu_") + (is_pt ? "pt_" : "eta_") + (is_loose ? "loose" : "tight");
    c.Print((path+".png").c_str());
}

std::unique_ptr<TH2D> Processor::Smooth2Dhisto(const TH2D* histo) const {
    std::vector<double> etaBins;
    for (int ibin = 1; ibin <= histo->GetNbinsY() + 1; ++ibin) {
        const double edge = histo->GetYaxis()->GetBinLowEdge(ibin);
        etaBins.emplace_back(edge);
    }

    const double min = histo->GetXaxis()->GetBinLowEdge(1);
    const double max = histo->GetXaxis()->GetBinUpEdge(histo->GetNbinsX() - 1);
    int nbins = static_cast<int>(max-min);
    std::vector<double> ptBins(nbins+1);
    for (int ibin = 0; ibin <= nbins; ++ibin) {
        const double edge = min + ibin;
        ptBins.at(ibin) = edge;
    }
    ptBins.emplace_back(histo->GetXaxis()->GetBinUpEdge(histo->GetNbinsX()));

    std::unique_ptr<TH2D> result = std::make_unique<TH2D>("", "", ptBins.size() - 1, ptBins.data(), etaBins.size() - 1, etaBins.data());

    for (int ibinx = 1; ibinx <= result->GetNbinsX(); ++ibinx) {
        for (int ibiny = 1; ibiny <= result->GetNbinsY(); ++ibiny) {
            const double centreX = result->GetXaxis()->GetBinCenter(ibinx);
            const double centreY = result->GetYaxis()->GetBinCenter(ibiny);
            const double value = histo->Interpolate(centreX, centreY);
            result->SetBinContent(ibinx, ibiny, value);
        }
    }

    return result;
}

std::unique_ptr<TH1D> Processor::Smooth1Dhisto(const TH1D* histo) const {

    const double min = histo->GetXaxis()->GetBinLowEdge(1);
    const double max = histo->GetXaxis()->GetBinUpEdge(histo->GetNbinsX() - 1);
    int nbins = static_cast<int>(max-min);
    std::vector<double> bins(nbins+1);
    for (int ibin = 0; ibin <= nbins; ++ibin) {
        const double edge = min + ibin;
        bins.at(ibin) = edge;
    }
    bins.emplace_back(histo->GetXaxis()->GetBinUpEdge(histo->GetNbinsX()));

    std::unique_ptr<TH1D> result = std::make_unique<TH1D>("", "", bins.size() - 1, bins.data());

    for (int ibinx = 1; ibinx <= result->GetNbinsX(); ++ibinx) {
        const double centreX = result->GetXaxis()->GetBinCenter(ibinx);
        const double value = histo->Interpolate(centreX);
        result->SetBinContent(ibinx, value);
    }

    return result;
}

void Processor::Correct2DHisto(TH2D* input) const {
    const int ybins = input->GetNbinsY();
    const int xbins = input->GetNbinsX();

    // skip the first bin
    for (int xbin = 2; xbin <= xbins; ++xbin) {
        for (int ybin = 1; ybin <= ybins; ++ybin) {
            const double content = input->GetBinContent(xbin, ybin);
            if (content > 1e-6) continue;

            // take the value from left bin in pT
            const double correction = input->GetBinContent(xbin-1, ybin);
            const double error = input->GetBinError(xbin-1, ybin);
            input->SetBinContent(xbin, ybin, correction);
            input->SetBinError(xbin, ybin, error);
        }
    }
}
